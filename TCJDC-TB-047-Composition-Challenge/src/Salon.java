
public class Salon {

	private Bed theBed;
	private Closet theCloset;

	public Salon(Bed theBed, Closet theCloset) {
		super();
		this.theBed = theBed;
		this.theCloset = theCloset;
	}

	public void inviteGuests() {

		getTheCloset().openDoors(2);
		System.out.println("Put clothes into closet");
	}
	
	public void putGuestsToSleep () {
		
		theBed.makeBed();
		
	}

	public Bed getTheBed() {
		return theBed;
	}

	public Closet getTheCloset() {
		return theCloset;
	}

	
	
}
