
public class Main {

	public static void main(String[] args) {

		Bed bed1 = new Bed(1, 2);
		Closet closet1 = new Closet(2, 3);
		Salon salon1 = new Salon(bed1, closet1);

		salon1.inviteGuests();
		
		salon1.getTheBed().makeBed();
	}

}
