
public class Closet {

	private int doors;
	private int shelves;

	public Closet(int doors, int shelves) {
		super();
		this.doors = doors;
		this.shelves = shelves;
	}

	public void openDoors(int doors) {

		System.out.println("Number of doors opened: " + doors);

	}

	public void closeDoors(int doors) {

		System.out.println("Number of doors closed: " + doors);

	}

	public int getNumberOfDoors() {
		return doors;
	}

	public int getNumberOfShelves() {
		return shelves;
	}

}
