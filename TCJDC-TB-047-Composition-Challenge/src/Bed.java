
public class Bed {
	private int width;
	private int height;

	public Bed(int width, int height) {
		super();
		this.width = width;
		this.height = height;
	}

	
	public void makeBed () {
		
		System.out.println("Making bed");
		
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

}
